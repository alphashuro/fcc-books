import React from 'react';
import { Link } from 'react-router';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

export default ({ open, onToggle }) => (
    <div>
        <Drawer 
            open={open}
            docked={false}
            onRequestChange={onToggle}
        >
        <MenuItem onClick={onToggle} containerElement={<Link to="/books/my" />}>
            My Books
        </MenuItem>
        <MenuItem onClick={onToggle} containerElement={<Link to="/books/all" />}>
            All Books
        </MenuItem>
        <MenuItem onClick={onToggle} containerElement={<Link to="/profile" />}>
            Settings
        </MenuItem>
        </Drawer>
    </div>
)