import React, { PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Menu from './Menu';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  static contextTypes = {
    router: PropTypes.object.isRequired,
  }

  handleToggleMenu = () => this.setState({ open: !this.state.open });

  render() { 
    return (
      <MuiThemeProvider>
        <div>
          <AppBar 
            title="Book Club" 
            onLeftIconButtonTouchTap={ this.handleToggleMenu }
            onTitleTouchTap={ () => this.context.router.push('/') }
          />
          <Menu 
            open={ this.state.open }
            onToggle={this.handleToggleMenu}
          />
          { this.props.children }
        </div>
      </MuiThemeProvider>
    );
  }
}