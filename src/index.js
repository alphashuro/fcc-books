import firebase from 'firebase/app';
firebase.initializeApp({
  apiKey: "AIzaSyBL3PjX2OZYaG9X8kKGChZZc5EBR-3a7sI",
  authDomain: "fcc-books.firebaseapp.com",
  databaseURL: "https://fcc-books.firebaseio.com",
  storageBucket: "fcc-books.appspot.com",
  messagingSenderId: "552893236532",
});

import './main.css';

import React from 'react';
import ReactDOM from 'react-dom';

import { IndexRoute, Router, Route, browserHistory } from 'react-router'
import App from './App'
import Login from './login'
import Home from './Home'

import auth from 'firebase/auth'

function requireAuth(nextState, replace) {
  if (!auth().currentUser) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

auth().onAuthStateChanged(function(user) {
  if (user) {
    browserHistory.push('/')
  } else {
    browserHistory.push('/login')
  }
})

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} onEnter={requireAuth} />
      <Route path="login" component={Login} />
     {/* <Route path="about" component={About} onEnter={requireAuth} />
      <Route path="inbox" component={Inbox}  onEnter={requireAuth}/>*/}
    </Route>
  </Router>,
  document.getElementById('root')
);
