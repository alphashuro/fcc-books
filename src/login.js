import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import auth from 'firebase/auth';

const provider = new auth.GoogleAuthProvider();

export default React.createClass({
    contextTypes: {
        router: React.PropTypes.object,
    },

    async componentDidMount() {
        try {
            const { user } = await auth().getRedirectResult();
            if (user) {
                this.context.router.push('/');
            }
        } catch (e) {
            console.log(e);
        }
    },
    
    render () {
        return (
            <div>
                <RaisedButton label="Signin with Google" onClick={() => auth().signInWithRedirect(provider)} />
            </div>
        );
    }
});